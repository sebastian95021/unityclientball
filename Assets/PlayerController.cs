﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxSpeed = 200f;
    public float speed = 200f;
    private Rigidbody2D rb2d;
    private Animator anim;
    public NetworkClient networkclient;
    private PlayerController pc;
    private Vector2 oldPosition;

    // Start is called before the first frame update
    void Start()
    {
        //Capturar script actual
        pc = GetComponent<PlayerController>();
        networkclient = FindObjectOfType<NetworkClient>();
        oldPosition = new Vector2(0, 0);

        //La variable playerName solo se usa para verificar y apropiarse de un de los objetos de jugador
        string playerName = "player" + NetworkClient.noplayer;
        Debug.Log("Mi numero de jugador global: " + playerName);
        int noplayer = Int32.Parse(gameObject.name.Substring(gameObject.name.Length -1, 1));
        Debug.Log("Soy el objeto: " + noplayer);
        if (gameObject.name != playerName)
        {
            pc.enabled = false;
            Debug.Log("Al objeto: " + noplayer + "le voy a asignar el player_id" + NetworkClient.allPlayersRoom[noplayer - 1]);
            gameObject.name = "player" + NetworkClient.allPlayersRoom[noplayer-1];
        }
        else
        {
            gameObject.name = "player" + NetworkClient.player_id;
        }
        


        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("speed", Mathf.Abs(rb2d.velocity.x));
    }

    void FixedUpdate()
    {
        //Simular friccion
        Vector3 fixedVelocity = rb2d.velocity;
        fixedVelocity.x *= 0.75f;
        rb2d.velocity = fixedVelocity;

        //Capturar entrada
        float h = Input.GetAxis("Horizontal");

        rb2d.AddForce(Vector2.right * speed * h);

        float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
        rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);

        //Voltear player dependiendo de la entrada
        /*if (h > 0.1f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        if (h < 0.1f)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }*/


        Vector2 newPosition = new Vector2(rb2d.position.x, rb2d.position.y);
        if (oldPosition != newPosition)
        {
            //Capturar datos de mi posicion actual para enviarlos al servidor
            networkclient.sendPlayerPosition(NetworkClient.player_id, Math.Round(rb2d.position.x, 2), Math.Round(rb2d.position.y, 2));
            oldPosition = newPosition;
        }
    }
}
