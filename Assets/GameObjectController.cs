﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectController : MonoBehaviour
{
    public GameObject[] players;

    // Start is called before the first frame update
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ObjectController(int id, float x, float y)
    {
        string playerName = "player" + id;
        foreach (GameObject player in players)
        {
            if (player.name == playerName)
            {
                Debug.Log("al: "+ playerName +" ponlo en x:" +x+" y: "+y);
                player.transform.position = new Vector3(x, y, 0);
            }
        }
    }
}
