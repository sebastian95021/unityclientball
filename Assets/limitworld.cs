﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class limitworld : MonoBehaviour
{
    public Text lifes;

    // Start is called before the first frame update
    void Start()
    {
        lifes.text = "10";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        int totalLifes = System.Int32.Parse(lifes.text);
        lifes.text = (totalLifes - 1).ToString();

    }
}
