﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    public NetworkClient nc;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void QueuePlayer()
    {
        int player_id = NetworkClient.player_id;
        nc.sendPlayerQueue(player_id);
    }
}
