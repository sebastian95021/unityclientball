﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class random_ball : MonoBehaviour
{
    public float maxSpeed = 600f;
    public float speed = 500f;
    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        int option = Random.Range(1, 4);
        Vector2 to = new Vector2(1f, 0.5f);

        switch (option)
        {
            case 1:
                to = new Vector2(1f, 0.5f);
                break;
            case 2:
                to = new Vector2(-1f, 0.5f);
                break;
            case 3:
                to = new Vector2(1f, -0.5f);
                break;
            case 4:
                to = new Vector2(-1f, -0.5f);
                break;
            default:
                Debug.Log("No se pudo encontrar una opcion de fuerza para la bola");
                break;
        }

        //Agregar empuje al aparecer la bola
        rb2d.AddForce(to * speed);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {

        //Limitar la velocidad de la bola
        float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -maxSpeed, maxSpeed);
        rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
    }
}

