﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createBall : MonoBehaviour
{
    public GameObject balls;
    public float creationtime = 5f, creationrange = 2f;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Create", 0.0f, creationtime);
        creationtime -= 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Create()
    {
        Vector3 spawnPosition = new Vector3(0, 0, 0);

        GameObject ball = Instantiate(balls, spawnPosition, Quaternion.identity);

    }
}
