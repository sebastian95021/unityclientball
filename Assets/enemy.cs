﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public float visionRadius = 7f, speed = 4f;
    GameObject[] balls;
    GameObject ball;
    Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        ball = FindClosestBall();

        Vector3 target = initialPosition;
        float dist = Vector3.Distance(ball.transform.position, transform.position);
        if (dist < visionRadius){
            target = new Vector3(ball.transform.position.x, transform.position.y, 0);
        }

        float fixedSpeed = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, fixedSpeed);

        Debug.DrawLine(transform.position, target, Color.green);
    }

    public GameObject FindClosestBall()
    {
        balls = GameObject.FindGameObjectsWithTag("ball");
        GameObject ballClosest = null;
        float distance = Mathf.Infinity;
        foreach (GameObject ball in balls)
        {
            Vector3 diff = ball.transform.position - initialPosition;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                ballClosest = ball;
                distance = curDistance;
            }
        }

        return ballClosest;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

}
