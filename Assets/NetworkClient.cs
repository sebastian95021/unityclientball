﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using SocketIO;
using System.Globalization;
using System.Threading;

public class NetworkClient : SocketIOComponent
{
    public GameObjectController goc = null;
    public static int player_id;
    public static int noplayer;
    public static string[] allPlayersRoom;


    public override void Start()
    {
        DontDestroyOnLoad(gameObject);
        base.Start();
        setupEvents();
    }

    public override void Update()
    {
        base.Update();
    }

    private void setupEvents()
    {
        On("open", (E) =>
        {
            Debug.Log("Connection made to the server");
        });

        On("OtherPlayersPosition", (E) =>
        {
            try
            {
                if (goc == null)
                    goc = FindObjectOfType<GameObjectController>();

                int id = Int32.Parse(E.data["id"].ToString());
                float x = float.Parse(E.data["x"].ToString().Trim('"'));
                float y = float.Parse(E.data["y"].ToString().Trim('"'));
                goc.ObjectController(id, x, y);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

        });

        On("playerId", (E) =>
        {
            int id = Int32.Parse(E.data["player_id"].ToString());
            PlayerId(id);
            Debug.Log("player_id: " + id);
        });

        On("startMatch", (E) =>
        {
            int room_id = Int32.Parse(E.data["room_id"].ToString());
            Debug.Log(E.data["allPlayersRoom"].ToString().Trim('"'));
            allPlayersRoom = E.data["allPlayersRoom"].ToString().Trim('"').Split(',');
            Debug.Log("room_id: " + room_id);
            SceneManager.LoadScene("PlayScene");
        });
        
        On("NoPlayer", (E) =>
        {
            int noplayer = Int32.Parse(E.data["noplayer"].ToString());
            NoPlayer(noplayer);
            Debug.Log("player number: " + noplayer);
        });

        On("disconnected", (E) =>
        {
            Debug.Log("Player has disconnected");
        });
    }

    public static void PlayerId(int id)
    {
        player_id = id;
    }

    public static void NoPlayer(int numplayer)
    {
        noplayer = numplayer;
    }

    public void sendPlayerQueue(int id)
    {
        Id player = new Id{id = id};
        Emit("playerQueue", new JSONObject(JsonUtility.ToJson(player)));
    }

    public void sendPlayerPosition(int id, double x, double y)
    {
        Player player = new Player
        {
            id = id,
            x = x.ToString(),
            y = y.ToString()
        };

        //Debug.Log(new JSONObject(JsonUtility.ToJson(player)));

        Emit("playerPosition", new JSONObject(JsonUtility.ToJson(player)));
    }
    
    [Serializable]
    public class Player
    {
        public int id;
        public string x;
        public string y;
    }

    [Serializable]
    public class Id
    {
        public int id;
    }
}